const MDCList = mdc.list.MDCList;
const MDCRipple = mdc.ripple.MDCRipple;
const list = new MDCList(document.querySelector(".mdc-list"));

const listItemRipples = list.listElements.map(
  (listItemEl) => new MDCRipple(listItemEl)
);
